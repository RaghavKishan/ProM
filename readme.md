# ProM - Project Management 

#### Goals: The goal of the application is to assist user/students to plan, coordinate and track projects/mini projects with their team/peers.

+ The user can sign up which registers him/her on the Firebase service. 
+ They can then create a project and add tasks to it.
+ These tasks can then be assigned an owner.

There are three categories under which a taks can fall.

| Categories|
| ------------- |
| New Taks |
| In Progress |
| Completed |

The owner can move the task from one cateory to the other.

All the user can view the current status of each tasks as the app is updated instantly.


### Few Snap Shots 
---

![alt text](ProM1.png) ![alt text](ProM2.png)

---

![alt text](ProM3.png) ![alt text](ProM4.png)

---

![alt text](ProM5.png)

