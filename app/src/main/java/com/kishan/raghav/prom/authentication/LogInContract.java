package com.kishan.raghav.prom.authentication;

import android.app.Activity;

import com.google.firebase.auth.FirebaseUser;
import com.kishan.raghav.prom.BasePresenter;
import com.kishan.raghav.prom.BaseService;
import com.kishan.raghav.prom.BaseView;

/**
 * Created by raghav on 2/25/18.
 */

public interface LogInContract {

    interface View{

        void updateUI();

        void updateUIInvalidCredentials();
    }

    interface Presenter extends BasePresenter{

        void signUp(String email, String password);

        void signIn(String email, String password);

        void checkCurrentUserSignInStatus();

        void signInSuccessful(FirebaseUser user);

    }

//    interface FireBaseServices extends BaseService<Presenter>{
//
//        // updateUI(FirebaseUser user);
//
//    }

}
