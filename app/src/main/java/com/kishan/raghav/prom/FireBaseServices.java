package com.kishan.raghav.prom;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kishan.raghav.prom.authentication.LogInContract;
import com.kishan.raghav.prom.authentication.LogInPresenter;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by raghav on 2/28/18.
 */

public class FireBaseServices{

    private FirebaseAuth mAuth;

    private static final String TAG = "FireBaseServicesTag";

    LogInPresenter logInPresneter;

    public FireBaseServices(LogInPresenter logInPresneter) {
        mAuth = FirebaseAuth.getInstance();
        this.logInPresneter = logInPresneter;
    }

    // If the User has not logged off, then this method returns the current logged in User.
    public FirebaseUser getCurrentUser(){

        FirebaseUser currentUser = mAuth.getCurrentUser();
        return currentUser;
    }



    public void signUpService(String email, String password, Activity context){

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
                    //@Override

                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            updateUI((String)null);
                        }

                        // ...
                    }
                });
    }


    public void updateUI(String failedSignUpResult){


    }


    public void updateUI(FirebaseUser user){

        logInPresneter.signInSuccessful(user);
    }

    public void signInService(String email, String password, Activity context){

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            updateUI((String)null);
                        }

                        // ...
                    }
                });
    }

    public void logOutService(){
        FirebaseAuth.getInstance().signOut();
        logInPresneter.signOutSuccessful();
    }

}
