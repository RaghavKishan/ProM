package com.kishan.raghav.prom.authentication;

import android.app.Activity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kishan.raghav.prom.FireBaseServices;

/**
 * Created by raghav on 2/27/18.
 */

public class LogInPresenter implements LogInContract.Presenter{

    FireBaseServices fireBaseService;

    LogInFragment logInFragment;

    StartUpActivity startUpActivity;

    Activity context;



    public LogInPresenter(Activity context, StartUpActivity view) {
        this.context = context;
        startUpActivity = view;
        //logInFragment.setPresenter(this);
        fireBaseService = new FireBaseServices(this);


    }

//
//    public LogInPresenter(FireBaseServices fireBaseService) {
//        this.fireBaseService = fireBaseService;
//    }

    //Interface methods

    @Override
    public void signUp(String email, String password) {

        fireBaseService.signUpService(email,password,context);
    }

    @Override
    public void signIn(String email, String password) {

        fireBaseService.signInService(email,password,context);

    }

    @Override
    public void checkCurrentUserSignInStatus() {

        FirebaseUser currentUser;

        //fireBaseService = new FireBaseServices();
        currentUser = fireBaseService.getCurrentUser();
        if (currentUser != null) {
            startUpActivity.currentUserSignInStatus(currentUser);
        }else
        {
            startUpActivity.currentUserSignInStatusNull();
        }
    }




    @Override
    public void start() {

    }

    //FireBase services methods
    // Used by Firebase Services.

    // update UI once the user has signed in or signed up.
    @Override
    public void signInSuccessful(FirebaseUser user) {
        //update UI function if user has signed in.
        startUpActivity.updateUI();
    }



    public void updateUI(String failedSignUpReference){

    }

    public void logOut(){
        fireBaseService.logOutService();
    }

    public void signOutSuccessful(){
        startUpActivity.updateUiPostSignOut();
    }

}
