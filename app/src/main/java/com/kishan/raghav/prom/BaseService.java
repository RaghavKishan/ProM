package com.kishan.raghav.prom;

/**
 * Created by raghav on 3/4/18.
 */

public interface BaseService<T> {

    void setPresenter(T presenter);
}
