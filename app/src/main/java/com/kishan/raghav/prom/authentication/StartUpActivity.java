package com.kishan.raghav.prom.authentication;

import android.app.Activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.kishan.raghav.prom.MainActivity;
import com.kishan.raghav.prom.R;
import com.kishan.raghav.prom.util.ActivityUtils;

import static com.google.common.base.Preconditions.checkNotNull;

public class StartUpActivity extends AppCompatActivity implements LogInContract.View {

    private LogInPresenter mPresenter;

//    private LogInContract.Presenter mContractPresenter;

    private ViewPager viewPager;

    FrameLayout startUpActivityFragmentContainer;

    RelativeLayout IntroPagerLayout;

    LinearLayout layoutDots;

    private int[] layouts;

    private TextView[] dots;

    private MyViewPagerAdapter myViewPagerAdapter;

    Button pagerNextButton, pagerLogInButton, logInButton, signUpButton, ChangeToLoginPageButton, ChangeToSignUpPageButton, log_out_button;

    EditText logInEmailEditText, logInPasswordEditText, signUpEmailEditText, signUpPasswordEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        log_out_button = (Button) findViewById(R.id.log_out_button);


//        startUpActivityFragmentContainer = findViewById(R.id.startUpActivityFragmentContainer);
//        IntroPagerLayout =  findViewById(R.id.IntroPagerLayout);
//        layoutDots = findViewById(R.id.layoutDots);
//        viewPager = (ViewPager) findViewById(R.id.introViewPager);
//        pagerNextButton = (Button) findViewById(R.id.pagerNextButton);
//        pagerLogInButton = (Button) findViewById(R.id.pagerLogInButton);

        // creating a reference to the presenter. Also passing an instance of the view.
        mPresenter = new LogInPresenter(
                (Activity) StartUpActivity.this,
                this
        );

        // Checking to see if user has already logged in and updating the view appropriately.
        mPresenter.checkCurrentUserSignInStatus();

    }

    protected void currentUserSignInStatus(FirebaseUser user) {
        mPresenter.signInSuccessful(user);
    }

    protected void currentUserSignInStatusNull() {

        //Removing  Title bar/navigation bar and making it a full screen.
//        if (Build.VERSION.SDK_INT >= 21) {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//        }

        setContentView(R.layout.welcome_slides_layout); //setting the appropriate xml layout

        layoutDots = findViewById(R.id.layoutDots);
        viewPager = (ViewPager) findViewById(R.id.introViewPager);
        pagerNextButton = (Button) findViewById(R.id.pagerNextButton);
        pagerLogInButton = (Button) findViewById(R.id.pagerLogInButton);

        layouts = new int[]{
                R.layout.welcome_slide_one,
                R.layout.welcome_slide_two,
                R.layout.welcome_slide_three};

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
        //changeStatusBarColor();


        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        pagerLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLogIn();
            }
        });

        pagerNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                    onClickSignUp();
                }
            }
        });

    }

    //menu



    // Intro Pager Code

    private void addBottomDots(int currentPage) {

        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        layoutDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            layoutDots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    //View pager adapter
    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                pagerNextButton.setText(getString(R.string.sign_up_button_string));
                pagerLogInButton.setVisibility(View.GONE);
            } else {
                // still pages are left
                pagerNextButton.setText(getString(R.string.next_string));
                pagerLogInButton.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    // Log In Contract View Methods implemented.

    @Override
    public void updateUI() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void updateUIInvalidCredentials() {

    }

    // Sign up on CLick.
    protected void onClickSignUp() {

        setContentView(R.layout.sign_up_layout);
        setTitle("Sign Up @ ProM");

        signUpButton = (Button) findViewById(R.id.signUpButton);
        signUpEmailEditText = (EditText) findViewById(R.id.signUpEmailEditText);
        signUpPasswordEditText = (EditText) findViewById(R.id.signUpPasswordEditText);
        ChangeToLoginPageButton = (Button) findViewById(R.id.ChangeToLoginPageButton);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String signUpEmailInputString, signUpPasswordInputString;
                signUpEmailInputString = signUpEmailEditText.getText().toString();
                signUpPasswordInputString = signUpPasswordEditText.getText().toString();
                mPresenter.signUp(signUpEmailInputString, signUpPasswordInputString);
            }
        });

        ChangeToLoginPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLogIn();
            }
        });
    }

    protected void onClickLogIn() {

        setContentView(R.layout.log_in_layout);
        setTitle("Log In @ ProM");

        logInButton = (Button) findViewById(R.id.logInButton);
        logInEmailEditText = (EditText) findViewById(R.id.logInEmailEditText);
        logInPasswordEditText = (EditText) findViewById(R.id.logInPasswordEditText);
        ChangeToSignUpPageButton = (Button) findViewById(R.id.ChangeToSignUpPageButton);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String logInEmailInputString, logInPasswordInputString;
                logInEmailInputString = logInEmailEditText.getText().toString();
                logInPasswordInputString = logInPasswordEditText.getText().toString();
                mPresenter.signIn(logInEmailInputString, logInPasswordInputString);

            }
        });

        ChangeToSignUpPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSignUp();
            }
        });
    }

    public void updateUiPostSignOut(){
        currentUserSignInStatusNull();
    }

}
