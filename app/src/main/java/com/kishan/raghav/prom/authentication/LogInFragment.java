package com.kishan.raghav.prom.authentication;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.kishan.raghav.prom.MainActivity;
import com.kishan.raghav.prom.R;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple {@link Fragment} subclass.
 */
public class LogInFragment extends Fragment implements LogInContract.View{

    private EditText mLogInEmailEditText, mLogInPasswordEditText, mSignUpNameEditText;

    private Button mLoginButton, mSignUpButton;

    private LogInContract.Presenter mPresenter;

    private String mInputEmail,mInputPassword,mInputName;



    public LogInFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment and set the references for the layout elements.
        View mLogInFragmentRoot = inflater.inflate(R.layout.fragment_log_in, container, false);

        mLogInEmailEditText = mLogInFragmentRoot.findViewById(R.id.logInEmailEditText);
        mLogInPasswordEditText = mLogInFragmentRoot.findViewById(R.id.logInPasswordEditText);
        mLoginButton = mLogInFragmentRoot.findViewById(R.id.logInButton);

        mSignUpNameEditText = mLogInFragmentRoot.findViewById(R.id.signUpNameEditText);
        mSignUpButton = mLogInFragmentRoot.findViewById(R.id.signUpButton);

        return mLogInFragmentRoot;
    }

    // this function should take the user to the main page.
    @Override
    public void updateUI() {
        Intent intent =  new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void updateUIInvalidCredentials() {

    }

//    @Override
//    public void setPresenter(LogInContract.Presenter presenter) {
//        mPresenter = checkNotNull(presenter);
//    }

    //write onlcik listener and execute  for sign up and execute the signup method.

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mInputEmail = mLogInEmailEditText.getText().toString();
        mInputName = mSignUpNameEditText.getText().toString();
        mInputPassword = mLogInPasswordEditText.getText().toString();

        //Calling Presenter SignUp() function on click of sign up
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.signUp(mInputEmail,mInputPassword);
            }
        });

    }

    //public void
}
